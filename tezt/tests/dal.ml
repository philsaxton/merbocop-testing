(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* Testing
   -------
   Component:    Data-availability layer
   Invocation:   dune exec tezt/tests/main.exe -- --file dal.ml
   Subject: Integration tests related to the data-availability layer
*)

let test_feature_flag =
  (* This test ensures the feature flag works:

     - 1. It checks the feature flag is not enabled by default

     - 2. It checks the new operations added by the feature flag
     cannot be propagated by checking their classification in the
     mempool. *)
  let open Tezt_tezos in
  Protocol.register_test ~__FILE__ ~title:"dal feature flag" ~tags:["dal"]
  @@ fun protocol ->
  let* node, client = Client.init_with_protocol `Client ~protocol () in
  let* protocol_parameters = RPC_legacy.get_constants client in
  let feature_flag =
    JSON.(
      protocol_parameters |-> "dal_parametric" |-> "feature_enable" |> as_bool)
  in
  let number_of_slots =
    JSON.(
      protocol_parameters |-> "dal_parametric" |-> "number_of_slots" |> as_int)
  in
  Check.(
    (feature_flag = false)
      bool
      ~error_msg:"Feature flag for the DAL should be disabled") ;
  let* (`OpHash oph1) =
    Operation.(
      forge_and_inject_operation
        ~force:true
        ~batch:
          (`Consensus
            (Dal_slot_availability
               {
                 endorser = Constant.bootstrap1.public_key_hash;
                 endorsement = Array.make number_of_slots false;
               }))
        ~signer:Constant.bootstrap1
        client)
  in
  let* slot_header_operation =
    Operation.mk_publish_slot_header
      ~source:Constant.bootstrap1
      ~index:0
      ~level:1
      ~header:0
      client
  in
  let* (`OpHash oph2) =
    Operation.(
      forge_and_inject_operation
        ~force:true
        ~batch:(`Manager [slot_header_operation])
        ~signer:Constant.bootstrap1
        client)
  in
  let* mempool = Mempool.get_mempool client in
  let expected_mempool = Mempool.{empty with refused = [oph1; oph2]} in
  Check.(
    (mempool = expected_mempool)
      Mempool.classified_typ
      ~error_msg:"Expected mempool: %R. Got: %L. (Order does not matter)") ;
  let* () = Client.bake_for_and_wait client in
  let* block_metadata = RPC.(call node @@ get_block_metadata ()) in
  let slot_availability =
    JSON.(block_metadata |-> "dal_slot_availability" |> is_null)
  in
  Check.(
    (slot_availability = true)
      bool
      ~error_msg:"Did not expect to find \"dal_slot_availibility\"") ;
  unit

let register ~protocols = test_feature_flag protocols
